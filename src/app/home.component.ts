import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html'
})
export class HomeComponent {

    public name: string = 'Param Randhawa';

    constructor() { }

    ngOnInit() { }

    ngOnDestroy() { }
}
