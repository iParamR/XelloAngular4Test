import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { TooltipsComponent } from './tooltips.component';

const appRoutes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'tooltips-buttons',
        component: TooltipsComponent
    }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);