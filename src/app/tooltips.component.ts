import { Component, ElementRef, ViewChild } from '@angular/core';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  moduleId: module.id,
  selector: 'tooltips',
  templateUrl: `tooltips.component.html`,
  host: {
    '(document:click)': 'handleClick($event)',
    '(window:keydown)': 'hotkeys($event)'
  },
  providers: [NgbTooltipConfig]
})
export class TooltipsComponent {

  @ViewChild('t1') public tooltip1: NgbTooltip;
  @ViewChild('t2') public tooltip2: NgbTooltip;

  public elementRef: ElementRef;
  public tooltipConfig: NgbTooltipConfig;

  constructor(myElement: ElementRef, myConfig: NgbTooltipConfig) {
    this.elementRef = myElement;
    this.tooltipConfig = myConfig;
    this.tooltipConfig.placement = 'top';
    this.tooltipConfig.triggers = 'click';
  }

  public onButtonA_click(event: any): void {
    // event.stopPropagation();
    this.tooltip1.open();
    this.tooltip2.close();
  }

  public onButtonB_click(event: any): void {
    // event.stopPropagation();
    this.tooltipConfig.placement = 'bottom';
    this.tooltip1.close();
    this.tooltip2.open();
  }

  public handleClick(event: any): void {
    var clickedComponent = event.target;
    var inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (inside) {
      //console.log('inside');
    } else {
      //console.log('outside');
      this.tooltip1.close();
      this.tooltip2.close();
    }
  }

  public hotkeys(event: any): void {
    if (event.keyCode == 27) { // esc key
      this.tooltip1.close();
      this.tooltip2.close();
    }
  }
}
